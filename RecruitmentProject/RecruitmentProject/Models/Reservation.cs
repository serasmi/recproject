﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RecruitmentProject.Models
{
    public class Reservation
    {
        [Required]
        public string Id { get; set; }
        [Required]
        [MaxLength(10)]
        public string Code { get; set; }
        [Required]
        public DateTime CreationDate { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public DateTime CheckInDate { get; set; }
        [Required]
        public DateTime CheckOutDate { get; set; }
        [Required]
        public string Currency { get; set; }
        public decimal? Commision { get; set; }
        public string Source { get; set; }

        public ICollection<Guest> Guests { get; set; } = new HashSet<Guest>();
    }
}