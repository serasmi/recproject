﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RecruitmentProject.Models
{
    public class Guest
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Id { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PostCode { get; set; }
        public string PhoneNo { get; set; }
        public string Address { get; set; }
        public string City { get; set; }

        public ICollection<Reservation> Reservations { get; set; } = new HashSet<Reservation>();
    }
}