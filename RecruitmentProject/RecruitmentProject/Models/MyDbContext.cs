﻿using System.Data.Entity;

namespace RecruitmentProject.Models
{
    public interface MyDbContext
    {
        IDbSet<Guest> GetGuests();
        IDbSet<Reservation> GetReservations();
        void SaveChanges();
    }
}
