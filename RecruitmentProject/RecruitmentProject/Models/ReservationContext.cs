﻿using System;
using System.Data.Entity;

namespace RecruitmentProject.Models
{
    public class ReservationContext : DbContext, MyDbContext
    {
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Reservation> Reservations { get; set; }

        public IDbSet<Guest> GetGuests() => Guests;

        public IDbSet<Reservation> GetReservations() => Reservations;

        void MyDbContext.SaveChanges() => SaveChanges();
    }
}