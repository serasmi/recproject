﻿using RecruitmentProject.Models;
using System.Data.Entity;
using System.Web.Http;
using System.Collections.Generic;

namespace RecruitmentProject.Controllers.Api
{
    public class ReservationsController : ApiController
    {
        private MyDbContext myDbContext;

        public ReservationsController(MyDbContext _myDb)
        {
            myDbContext = _myDb;
        }

        [HttpGet]
        public IEnumerable<Reservation> Get()
        {
            return myDbContext.GetReservations();
        }
    }
}
