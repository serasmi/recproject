﻿using RecruitmentProject.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace RecruitmentProject.Controllers.Api
{
    public class GuestsController : ApiController
    {
        private MyDbContext myDbContext;

        public GuestsController(MyDbContext _myDb)
        {
            myDbContext = _myDb;
        }

        [HttpGet]
        public IEnumerable<Guest> GetPeter()
        {
            return myDbContext.GetGuests().Where(guest => string.Compare(guest.Name, "Piotr") == 0).Where(guest => string.Compare(guest.City, "Wrocław") == 0 || string.IsNullOrEmpty(guest.City) == true);
        }

        /*
            {
	            "Name":"Test",
	            "Surname":"Test",
	            "Id":"3",
	            "Email":"example5@test.com",
	            "Reservations":[
		            {
			            "Id":"8",
			            "Code":"0022",
			            "CreationDate":"2020-02-04 12:15:22.443",
			            "Price":10.0,
			            "CheckInDate":"2020-02-04 12:15:22.443",
			            "CheckOutDate":"2020-02-04 12:15:22.443",
			            "Currency":"Zl"
		            }
	            ]
            }
         */
        [HttpPost]
        public IHttpActionResult PostNewGuest([FromBody]Guest guest)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            myDbContext.GetGuests().Add(guest);

            myDbContext.SaveChanges();

            return Ok();
        }
    }
}
