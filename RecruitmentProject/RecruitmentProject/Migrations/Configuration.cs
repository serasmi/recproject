﻿namespace RecruitmentProject.Migrations
{
    using System.Data.Entity.Migrations;
    using RecruitmentProject.Models;
    using System;

    internal sealed class Configuration : DbMigrationsConfiguration<RecruitmentProject.Models.ReservationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RecruitmentProject.Models.ReservationContext context)
        {
            var firstReservation = new Reservation
            {
                Id = Guid.NewGuid().ToString(),
                Code = "1122334455",
                CreationDate = DateTime.Now,
                Price = 6.0M,
                CheckInDate = DateTime.Now,
                CheckOutDate = DateTime.Now,
                Currency = "t"
            };

            var secondReservation = new Reservation
            {
                Id = Guid.NewGuid().ToString(),
                Code = "2233445566",
                CreationDate = DateTime.Now,
                Price = 8.0M,
                CheckInDate = DateTime.Now,
                CheckOutDate = DateTime.Now,
                Currency = "t"
            };

            var thirdReservation = new Reservation
            {
                Id = Guid.NewGuid().ToString(),
                Code = "3344556677",
                CreationDate = DateTime.Now,
                Price = 10.0M,
                CheckInDate = DateTime.Now,
                CheckOutDate = DateTime.Now,
                Currency = "t"
            };

            var fourthReservation = new Reservation
            {
                Id = Guid.NewGuid().ToString(),
                Code = "4455667788",
                CreationDate = DateTime.Now,
                Price = 12.0M,
                CheckInDate = DateTime.Now,
                CheckOutDate = DateTime.Now,
                Currency = "t"
            };

            var firstGuest = new Guest
            {
                Id = Guid.NewGuid().ToString(),
                BirthDate = DateTime.Now,
                Email = "example2@test.com",
                Name = "Jane",
                Surname = "Doe",
                PostCode = "335577"
            };

            var secondGuest = new Guest
            {
                Id = Guid.NewGuid().ToString(),
                BirthDate = DateTime.Now,
                Email = "example2@test.com",
                Name = "Piotr",
                Surname = "Doe",
                PostCode = "335577"
            };

            var thirdGuest = new Guest
            {
                Id = Guid.NewGuid().ToString(),
                BirthDate = DateTime.Now,
                Email = "example2@test.com",
                Name = "Piotr",
                Surname = "Doe",
                PostCode = "335577",
                City = "Wrocław"
            };

            var fourthGuest = new Guest
            {
                Id = Guid.NewGuid().ToString(),
                BirthDate = DateTime.Now,
                Email = "example2@test.com",
                Name = "Piotr",
                Surname = "Doe",
                PostCode = "335577",
                City = "Bydgoszcz"
            };

            firstGuest.Reservations.Add(firstReservation);
            firstGuest.Reservations.Add(secondReservation);

            secondGuest.Reservations.Add(secondReservation);
            secondGuest.Reservations.Add(thirdReservation);

            thirdGuest.Reservations.Add(thirdReservation);
            thirdGuest.Reservations.Add(fourthReservation);

            fourthGuest.Reservations.Add(fourthReservation);
            fourthGuest.Reservations.Add(firstReservation);

            context.Guests.Add(firstGuest);
            context.Guests.Add(secondGuest);
            context.Guests.Add(thirdGuest);
            context.Guests.Add(fourthGuest);
            context.SaveChanges();
        }
    }
}
