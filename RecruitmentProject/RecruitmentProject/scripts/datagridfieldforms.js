﻿Ext.onReady(function () {
    Ext.define('Reservation', {
        extend: 'Ext.data.Model',
        fields: ['Id', 'Code', 'CreationDate', 'Price', 'CheckInDate', 'CheckOutDate', 'Currency', 'Commision', 'Source', 'Guest'],
        proxy: {
            type: 'rest',
            url: 'http://localhost:54365/api/reservations'
        }
    });

    var store = Ext.create('Ext.data.Store', {
        model: 'Reservation'
    });

    Ext.create('Ext.grid.Panel', {
        renderTo: Ext.getBody(),
        store: store.load(),
        storeId: 'mystore',
        title: 'Application Users',
        columns: [{
            text: 'Id',
            sortable: false,
            hideable: false,
            dataIndex: 'Id'
        }, {
            text: 'Reservation Code',
            dataIndex: 'Code'
        }, {
            text: 'Creation Date',
            dataIndex: 'CreationDate'
        }, {
            text: 'Price',
            dataIndex: 'Price'
        }, {
            text: 'Check-In Date',
            dataIndex: 'CheckInDate'
        }, {
            text: 'Check-Out Date',
            dataIndex: 'CheckOutDate'
        }, {
            text: 'Currency',
            dataIndex: 'Currency'
        }, {
            text: 'Commision',
            dataIndex: 'Commision'
        }, {
            text: 'Source',
            dataIndex: 'Source'
        }, {
            text: 'Guest',
            dataIndex: 'Guest'
        }]
    });

    return;

    var task = {
        run: function () {
            store.load()
        },
        interval: 15000 //15 seconds
    }

    var runner = new Ext.util.TaskRunner();

    //runner.start(task);
});