//using System.Web.Mvc;
//using Unity;
//using Unity.Mvc5;
//using RecruitmentProject.Models;
//using System.Web.Http;
using System.Web.Http;
using System.Web.Mvc;
using RecruitmentProject.Models;
using Unity;

namespace RecruitmentProject
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<MyDbContext, ReservationContext>();
            DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}